import React from "react";

import toJson from "enzyme-to-json";
import { shallow, mount } from "enzyme";
import ItemCard from "../src/components/Middle/itemCard";
import ItemSong from "../src/components/Middle/itemSong";

describe("Testing ItemCard component -> ", () => {
  it("Try to render 'ItemCard' correctly", () => {
    const props = {
      playlist: {
        item: {}
      },
      onDelete: jest.fn()
    };

    const wrapper = shallow(<ItemCard {...props} browser={false} />);

    expect(toJson(wrapper)).toMatchSnapshot();
    expect(wrapper.find("img").length).toBe(1);

    wrapper.find("#btnDelete").simulate("click");
    expect(props.onDelete.mock.calls.length).toEqual(1);
  });

  it("If contains title", () => {
    const props = {
      match: {
        params: {}
      }
    };

    const wrapper = shallow(<ItemCard {...props} browser={true} onDelete={jest.fn()} />);
    const value = wrapper.find("#titleBrowser").text();
    expect(value).toEqual("ALBUM");
  });

  it("If contains title", () => {
    const props = {
      playlist: {
        item: {}
      }
    };

    const wrapper = mount(<ItemCard {...props} browser={false} onDelete={jest.fn()} />);
    const value = wrapper.find("#titleList").text();
    expect(value).toEqual("PLAYLISTS");
  });
});

describe("Testing ItemSong component -> ", () => {
  it("Try to render 'ItemSong' correctly", () => {
    const props = {
      i: 0,
      obj: {},
      handleClick: jest.fn()
    };

    const wrapper = shallow(<ItemSong {...props} browser={false} />);
    expect(wrapper.find("#iconFavorite").length).toBe(1);
  });

  it("If is Browser", () => {
    const props = {
      i: 0,
      anchorEl: false,
      obj: {},
      playlists: {
        items: []
      },
      handlePlaylistSelect: jest.fn(),
      handleClose: jest.fn(),
      selectedItem: jest.fn(),
      handleClick: jest.fn()
    };

    const wrapper = shallow(<ItemSong {...props} browser={true} />);
    expect(wrapper.find("#btnHandleClick").length).toBe(1);

    wrapper.find("#btnHandleClick").simulate("click");
    expect(props.handleClick.mock.calls.length).toEqual(1);
  });

  it("If is List", () => {
    const props = {
      i: 0,
      obj: {},
      playlist: {
        items: []
      },
      handlePlaylistDelete: jest.fn()
    };

    const wrapper = shallow(<ItemSong {...props} browser={false} />);
    expect(wrapper.find("#nameArtist").length).toBe(1);

    wrapper.find("#btnHandlePlaylistDelete").simulate("click");
    expect(props.handlePlaylistDelete.mock.calls.length).toEqual(1);
  });
});
