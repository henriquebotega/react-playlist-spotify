import { getAll, getByID } from "../src/actions/songActions";
import { getAll as getAllPlaylists, getByID as getByIDPlaylists, remove, insert, update } from "../src/actions/playlistActions";

describe("Testing actions in songActions -> ", () => {
  it("Get return of 'getAll' in songs", () => {
    expect(getAll()).toEqual(expect.any(Function));
  });

  it("Get return of specific 'getByID' in songs", () => {
    expect(getByID(1)).toEqual(expect.any(Function));
  });
});

describe("Testing actions in playlistActions -> ", () => {
  it("Get return of 'getAll' in songs", () => {
    expect(getAllPlaylists()).toEqual(expect.any(Function));
  });

  it("Get return of specific 'getByID' in songs", () => {
    expect(getByIDPlaylists(1)).toEqual(expect.any(Function));
  });

  it("Get return of specific 'remove' in songs", () => {
    expect(remove({})).toEqual(expect.any(Function));
  });

  it("Get return of specific 'insert' in songs", () => {
    expect(insert({})).toEqual(expect.any(Function));
  });

  it("Get return of specific 'update' in songs", () => {
    expect(update({})).toEqual(expect.any(Function));
  });
});
