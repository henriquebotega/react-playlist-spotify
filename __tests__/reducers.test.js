import { songReducer } from "../src/reducers/songReducer";
import { playlistReducer } from "../src/reducers/playlistReducer";

describe("Testing reducers from songReducer-> ", () => {
  it("Call 'SONG_REQUEST_GETALL' into songReducer", () => {
    const initialState = {
      items: [],
      item: {},
      loading: false,
      error: false
    };

    const action = { type: "SONG_REQUEST_GETALL" };
    const state = songReducer(initialState, action);

    expect(state).toEqual({ loading: true, error: false, item: {}, items: [] });
  });

  it("Call 'SONG_SUCCESS_GETALL' into songReducer", () => {
    const initialState = {
      items: [],
      loading: true
    };

    const action = { type: "SONG_SUCCESS_GETALL" };
    const state = songReducer(initialState, action);

    expect(state).toEqual({ loading: false, items: undefined });
  });

  it("Call 'SONG_FAILURE_GETALL' into songReducer", () => {
    const initialState = {
      items: undefined,
      loading: true,
      error: false
    };

    const action = { type: "SONG_FAILURE_GETALL" };
    const state = songReducer(initialState, action);

    expect(state).toEqual({ items: [], loading: false, error: true });
  });

  it("Call 'SONG_REQUEST_BYID' into songReducer", () => {
    const initialState = {
      item: {},
      loading: false,
      error: false
    };

    const action = { type: "SONG_REQUEST_BYID" };
    const state = songReducer(initialState, action);

    expect(state).toEqual({ loading: true, error: false, item: {} });
  });

  it("Call 'SONG_SUCCESS_BYID' into songReducer", () => {
    const initialState = {
      item: {},
      loading: true,
      error: false
    };

    const action = { type: "SONG_SUCCESS_BYID" };
    const state = songReducer(initialState, action);

    expect(state).toEqual({ loading: false, error: false, item: undefined });
  });

  it("Call 'SONG_FAILURE_BYID' into songReducer", () => {
    const initialState = {
      item: {},
      loading: true,
      error: false
    };

    const action = { type: "SONG_FAILURE_BYID" };
    const state = songReducer(initialState, action);

    expect(state).toEqual({ item: {}, loading: false, error: true });
  });
});

describe("Testing reducers from playlistReducer-> ", () => {
  it("Call 'PLAYLIST_REQUEST_GETALL' into playlistReducer", () => {
    const initialState = {
      items: [],
      item: {},
      loading: false,
      error: false
    };

    const action = { type: "PLAYLIST_REQUEST_GETALL" };
    const state = playlistReducer(initialState, action);

    expect(state).toEqual({ loading: true, error: false, item: {}, items: [] });
  });

  // it("Call 'PLAYLIST_SUCCESS_GETALL' into playlistReducer", () => {
  //   const initialState = {
  //     items: [],
  //     loading: true
  //   };

  //   const action = { type: "PLAYLIST_SUCCESS_GETALL" };
  //   const state = playlistReducer(initialState, action);

  //   expect(state).toEqual({ loading: false, items: undefined });
  // });

  it("Call 'PLAYLIST_FAILURE_GETALL' into playlistReducer", () => {
    const initialState = {
      items: undefined,
      loading: true,
      error: false
    };

    const action = { type: "PLAYLIST_FAILURE_GETALL" };
    const state = playlistReducer(initialState, action);

    expect(state).toEqual({ items: [], loading: false, error: true });
  });

  it("Call 'PLAYLIST_REQUEST_BYID' into playlistReducer", () => {
    const initialState = {
      loading: false,
      item: {},
      error: true
    };

    const action = { type: "PLAYLIST_REQUEST_BYID" };
    const state = playlistReducer(initialState, action);

    expect(state).toEqual({ loading: true, error: false, item: {} });
  });

  it("Call 'PLAYLIST_SUCCESS_BYID' into playlistReducer", () => {
    const initialState = {
      loading: false,
      item: {},
      error: true
    };

    const action = { type: "PLAYLIST_SUCCESS_BYID" };
    const state = playlistReducer(initialState, action);

    expect(state).toEqual({ loading: false, error: false, item: undefined });
  });

  it("Call 'PLAYLIST_FAILURE_BYID' into playlistReducer", () => {
    const initialState = {
      loading: false,
      item: {},
      error: false
    };

    const action = { type: "PLAYLIST_FAILURE_BYID" };
    const state = playlistReducer(initialState, action);

    expect(state).toEqual({ loading: false, error: true, item: {} });
  });

  it("Call 'PLAYLIST_REQUEST_DELETE' into playlistReducer", () => {
    const initialState = {
      error: true
    };

    const action = { type: "PLAYLIST_REQUEST_DELETE" };
    const state = playlistReducer(initialState, action);

    expect(state).toEqual({ error: false });
  });

  // it("Call 'PLAYLIST_SUCCESS_DELETE' into playlistReducer", () => {
  //   const initialState = {
  //     error: true
  //   };

  //   const action = { type: "PLAYLIST_SUCCESS_DELETE" };
  //   const state = playlistReducer(initialState, action);

  //   expect(state).toEqual({ error: false });
  // });

  it("Call 'PLAYLIST_FAILURE_DELETE' into playlistReducer", () => {
    const initialState = {
      error: false
    };

    const action = { type: "PLAYLIST_FAILURE_DELETE" };
    const state = playlistReducer(initialState, action);

    expect(state).toEqual({ error: true });
  });

  it("Call 'PLAYLIST_REQUEST_INSERT' into playlistReducer", () => {
    const initialState = {
      error: true
    };

    const action = { type: "PLAYLIST_REQUEST_INSERT" };
    const state = playlistReducer(initialState, action);

    expect(state).toEqual({ error: false });
  });

  // it("Call 'PLAYLIST_SUCCESS_INSERT' into playlistReducer", () => {
  //   const initialState = {
  //     error: true
  //   };

  //   const action = { type: "PLAYLIST_SUCCESS_INSERT" };
  //   const state = playlistReducer(initialState, action);

  //   expect(state).toEqual({ error: false });
  // });

  it("Call 'PLAYLIST_FAILURE_INSERT' into playlistReducer", () => {
    const initialState = {
      error: false
    };

    const action = { type: "PLAYLIST_FAILURE_INSERT" };
    const state = playlistReducer(initialState, action);

    expect(state).toEqual({ error: true });
  });

  it("Call 'PLAYLIST_REQUEST_UPDATE' into playlistReducer", () => {
    const initialState = {
      error: true
    };

    const action = { type: "PLAYLIST_REQUEST_UPDATE" };
    const state = playlistReducer(initialState, action);

    expect(state).toEqual({ error: false });
  });

  // it("Call 'PLAYLIST_SUCCESS_UPDATE' into playlistReducer", () => {
  //   const initialState = {
  //     error: true
  //   };

  //   const action = { type: "PLAYLIST_SUCCESS_UPDATE" };
  //   const state = playlistReducer(initialState, action);

  //   expect(state).toEqual({ error: false });
  // });

  it("Call 'PLAYLIST_FAILURE_UPDATE' into playlistReducer", () => {
    const initialState = {
      error: false
    };

    const action = { type: "PLAYLIST_FAILURE_UPDATE" };
    const state = playlistReducer(initialState, action);

    expect(state).toEqual({ error: true });
  });
});
