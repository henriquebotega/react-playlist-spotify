import React from "react";
import ReactDOM from "react-dom";

import { Provider } from "react-redux";
import { store } from "./store";
import { SnackbarProvider } from "notistack";

import App from "./App";

function InitialScreen() {
  return (
    <Provider store={store}>
      <SnackbarProvider>
        <App />
      </SnackbarProvider>
    </Provider>
  );
}

ReactDOM.render(<InitialScreen />, document.getElementById("root"));
