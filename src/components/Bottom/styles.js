export const Container = {
  display: "flex",
  flexDirection: "row",
  height: "74px",
  paddingLeft: "10px",
  paddingRight: "10px",
  borderTop: "1px solid #000",
  backgroundColor: "#222222"
};

export const DescriptionPhoto = {
  fontSize: "12px"
};

export const TitlePhoto = {
  fontSize: "14px",
  fontWeight: "bold"
};

export const TextPhoto = {
  marginLeft: "10px"
};

export const BoxPhoto = {
  width: "200px",
  display: "flex",
  flexDirection: "row",
  alignItems: "center"
};

export const BoxButtonsPlayer = {
  width: "25%",
  display: "flex",
  alignItems: "center",
  justifyContent: "space-around"
};

export const BoxPlayer = {
  flex: 1,
  flexDirection: "column",
  display: "flex",
  alignItems: "center",
  justifyContent: "center"
};

export const BoxPlayerBar = {
  width: "90%",
  fontSize: "11px",
  textAlign: "center"
};

export const BarOne = {
  borderRadius: 5,
  display: "inline-flex",
  marginLeft: "5px",
  width: "20%",
  borderTopWidth: 1,
  lineHeight: "0",
  borderColor: "#777777",
  borderStyle: "solid"
};

export const BarTwo = {
  display: "inline-flex",
  marginRight: "5px",
  width: "50%",
  borderTopWidth: 1,
  lineHeight: "0",
  borderColor: "#444444",
  borderStyle: "solid"
};

export const BoxRight = {
  width: "200px",
  display: "flex",
  alignItems: "center",
  justifyContent: "space-around",
  color: "gray"
};

export const BarOneRigth = {
  borderRadius: 5,
  display: "inline-flex",
  marginLeft: "5px",
  width: "40px",
  borderTopWidth: 1,
  lineHeight: "0",
  borderColor: "#777777",
  borderStyle: "solid"
};

export const BarTwoRigth = {
  display: "inline-flex",
  marginRight: "5px",
  width: "10px",
  borderTopWidth: 1,
  lineHeight: "0",
  borderColor: "#444444",
  borderStyle: "solid"
};

export const font14 = {
  fontSize: "14px"
};
