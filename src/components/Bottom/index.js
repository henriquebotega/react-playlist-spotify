import React, { Component } from "react";

import { Container, BoxPhoto, BoxPlayer, TextPhoto, TitlePhoto, DescriptionPhoto, font14, BoxPlayerBar, BoxButtonsPlayer, BarOne, BarTwo, BoxRight, BarOneRigth, BarTwoRigth } from "./styles";

import { ArrowLeft, ArrowRight, PlayCircleOutline, Cached, Shuffle, VolumeUp, Phonelink, AspectRatio } from "@material-ui/icons";

const ContainerPhoto = () => {
  return (
    <div style={BoxPhoto}>
      <img alt="" src="https://source.unsplash.com/featured/?artist" width="50px" height="50px" />

      <div style={TextPhoto}>
        <div style={TitlePhoto}>Arcade Fire</div>
        <div style={DescriptionPhoto}>Rebellion (Lies)</div>
      </div>
    </div>
  );
};

const ContainerPlayer = () => {
  return (
    <div style={BoxPlayer}>
      <div style={BoxButtonsPlayer}>
        <Shuffle style={font14} />
        <ArrowLeft />
        <PlayCircleOutline />
        <ArrowRight />
        <Cached style={font14} />
      </div>

      <div style={BoxPlayerBar}>
        <span>1:00</span>
        <span style={BarOne}>&nbsp;</span>
        <span style={BarTwo}>&nbsp;</span>
        <span>5:12</span>
      </div>
    </div>
  );
};

const ContainerRight = () => {
  return (
    <div style={BoxRight}>
      <Phonelink style={font14} />
      <VolumeUp style={font14} />

      <span>
        <span style={BarOneRigth}>&nbsp;</span>
        <span style={BarTwoRigth}>&nbsp;</span>
      </span>

      <AspectRatio style={font14} />
    </div>
  );
};

class Bottom extends Component {
  render() {
    return (
      <div style={Container}>
        <ContainerPhoto />

        <ContainerPlayer />

        <ContainerRight />
      </div>
    );
  }
}

export default Bottom;
