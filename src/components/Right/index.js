import React, { Component } from "react";

import { Container, font12, boxFriend, photo, boxDescription, title, titleSong } from "./styles";

class Right extends Component {
  render() {
    const colSongs = [
      {
        user: "Anderson Botega",
        album: "The All-American Rejects",
        duration: 233,
        title: "Swing, Swing",
        id: 2,
        artist: "The All-American Rejects"
      },
      {
        user: "Hugo Fill",
        album: "Funeral",
        duration: 289,
        title: "Neighborhood #4 (Kettles)",
        id: 16,
        artist: "Arcade Fire"
      },
      {
        user: "Jack Frost",
        album: "Bloom",
        duration: 264,
        title: "Other People",
        id: 25,
        artist: "Beach House"
      },
      {
        user: "Felipe Durst",
        album: "Whatever and Ever Amen",
        duration: 292,
        title: "Smoke",
        id: 52,
        artist: "Ben Folds Five"
      },
      {
        user: "Carlos Silva",
        album: "Crimes [Bonus Track Version]",
        duration: 157,
        title: "Trash Flavored Trash",
        id: 74,
        artist: "The Blood Brothers"
      }
    ];

    return (
      <div style={Container}>
        <div style={title}>Friend activity</div>

        {colSongs.map((obj, i) => {
          return (
            <div key={i} style={boxFriend}>
              <img alt="" src={`https://source.unsplash.com/featured/?${obj.id}`} width="40px" height="40px" style={photo} />

              <div style={boxDescription}>
                <span style={titleSong}>{obj.user}</span>
                <span style={font12}>{obj.artist}</span>
                <span style={font12}>{obj.title}</span>
              </div>
            </div>
          );
        })}
      </div>
    );
  }
}

export default Right;
