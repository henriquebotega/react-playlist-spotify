export const Container = {
  height: "100%",
  overflowY: "auto",
  width: "250px",
  display: "flex",
  alignItems: "center",
  flexDirection: "column",
  backgroundColor: "#111111"
};

export const title = {
  display: "flex",
  color: "white",
  width: "90%",
  fontWeight: "bold",
  padding: "10px",
  borderBottomWidth: 1,
  borderBottomColor: "gray",
  borderBottomStyle: "solid"
};

export const photo = {
  marginRight: "10px",
  borderRadius: "50px"
};

export const boxFriend = {
  width: "200px",
  margin: "10px",
  display: "flex",
  flexDirection: "row",
  alignItems: "center"
};

export const titleSong = {
  fontSize: "14px",
  fontWeight: "bold"
};

export const boxDescription = {
  display: "flex",
  flexDirection: "column",
  alignItems: "start"
};
export const font12 = {
  fontSize: "12px"
};
