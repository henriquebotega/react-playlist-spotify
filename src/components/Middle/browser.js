import React, { Component } from "react";

import { connect } from "react-redux";

import { Link } from "react-router-dom";
import { browserMenu, friendAcitivy, boxFriendsDetails, boxFriendsDetailsTitle, boxFriendsSongs, quadroSong, ContainerBrowser, subMenu, opcaoMenu } from "./styles";

class BrowserPage extends Component {
  render() {
    const colSongs = [];

    if (this.props.songs.items.length > 0) {
      const collectionSongs = this.props.songs.items;

      collectionSongs.filter(function(el) {
        if (colSongs.length === 0) {
          colSongs.push(el);
          return true;
        } else {
          var jaExiste = false;

          colSongs.forEach(obj => {
            if (obj.album === el.album) {
              jaExiste = true;
            }
          });

          if (!jaExiste) {
            colSongs.push(el);
            return true;
          }
        }

        return false;
      });
    }

    return (
      <div style={ContainerBrowser}>
        <h2>Browser</h2>

        <div style={subMenu}>
          <span style={browserMenu}>OVERVIEW</span>
          <span style={browserMenu}>CHARTS</span>
          <span style={browserMenu}>GENRE & MOODS</span>
          <span style={browserMenu}>NEW RELEASES</span>
          <span style={browserMenu}>DISCOVER</span>
          <span style={browserMenu}>CONCERTS</span>
          <span style={browserMenu}>PODCASTS</span>
        </div>

        <div style={friendAcitivy}>Start your day</div>

        <div style={boxFriendsSongs}>
          {colSongs.map((obj, i) => {
            return (
              <div key={i} style={quadroSong}>
                <Link style={opcaoMenu} to={`/browserlist/${obj.album}`}>
                  <img alt="" src={`https://source.unsplash.com/featured/?${obj.id}`} width="175px" height="175px" style={{ marginRight: "10px" }} />
                </Link>

                <div style={boxFriendsDetails}>
                  <span style={boxFriendsDetailsTitle}>{obj.title}</span>
                  <span>{obj.album}</span>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  songs: state.songReducer
});

export default connect(mapStateToProps)(BrowserPage);
