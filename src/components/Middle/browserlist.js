import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { title, boxColSong } from "./styles";

import { update } from "../../actions/playlistActions";

import { withSnackbar } from "notistack";

import ItemCard from "./itemCard";
import ItemSong from "./itemSong";

class BrowserListPage extends Component {
  state = {
    anchorEl: null,
    selectedItem: null
  };

  handleClick = event => {
    this.setState({
      anchorEl: event.currentTarget
    });
  };

  handleClose = () => {
    this.setState({
      anchorEl: null,
      selectedItem: null
    });
  };

  selectedItem = obj => {
    this.setState({
      selectedItem: obj
    });
  };

  handlePlaylistSelect = item => {
    this.props.enqueueSnackbar("Playlist changed created", { variant: "info" });

    item.songs.push(this.state.selectedItem);
    this.props.update(item);
    this.handleClose();
  };

  render() {
    const { loading } = this.props.songs;
    const colSongs = [];

    if (this.props.match.params.album) {
      const collectionSongs = this.props.songs.items;
      const albumSongs = this.props.match.params.album;

      collectionSongs.filter(function(el) {
        if (albumSongs === el.album) {
          colSongs.push(el);
          return true;
        }

        return false;
      });
    }

    return (
      <div>
        <ItemCard {...this.props} browser={true} />

        {loading && <div>Wait...</div>}

        {colSongs && (
          <div style={boxColSong}>
            <div style={{ width: "75px" }}>&nbsp;</div>
            <div style={title}>TITLE</div>
            <div style={{ width: "50px" }}></div>
          </div>
        )}

        {colSongs && colSongs.map((obj, i) => <ItemSong key={i} {...this.props} anchorEl={this.state.anchorEl} handleClose={this.handleClose} handlePlaylistSelect={this.handlePlaylistSelect} handleClick={this.handleClick} selectedItem={this.selectedItem} browser={true} obj={obj} i={i} />)}
      </div>
    );
  }
}

const mapStateToProps = state => ({ songs: state.songReducer, playlists: state.playlistReducer });
const mapDispatchToProps = dispatch => bindActionCreators({ update }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withSnackbar(BrowserListPage));
