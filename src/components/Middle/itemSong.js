import React from "react";

import { boxSong, boxFavorite, titleSong, btnsPlaylist } from "./styles";

import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";

import { FavoriteBorder, AddOutlined, RemoveOutlined } from "@material-ui/icons";

const ItemSong = props => {
  return (
    <div style={boxSong}>
      <div style={boxFavorite}>
        <FavoriteBorder id="iconFavorite" style={{ fontSize: "15px" }} />
      </div>

      <div style={titleSong}>{props.obj.title}</div>

      {props.browser && (
        <>
          <div style={btnsPlaylist}>
            <AddOutlined
              id="btnHandleClick"
              onClick={e => {
                props.handleClick(e);
                props.selectedItem(props.obj.id);
              }}
            />

            <Menu anchorEl={props.anchorEl} open={Boolean(props.anchorEl)} onClose={props.handleClose}>
              {props.playlists.items.map((objMenu, iMenu) => {
                return (
                  <MenuItem key={iMenu} onClick={() => props.handlePlaylistSelect(objMenu)}>
                    {objMenu.name}
                  </MenuItem>
                );
              })}
            </Menu>
          </div>
        </>
      )}

      {!props.browser && (
        <>
          <div id="nameArtist" style={titleSong}>
            {props.obj.artist}
          </div>

          <div style={btnsPlaylist}>
            <RemoveOutlined id="btnHandlePlaylistDelete" onClick={() => props.handlePlaylistDelete(props.obj)} />
          </div>
        </>
      )}
    </div>
  );
};

export default ItemSong;
