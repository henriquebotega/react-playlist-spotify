import React from "react";

import { boxItemCard, title, btnDelete } from "./styles";

export default function itemCard(props) {
  return (
    <div style={boxItemCard}>
      <img alt="" src="https://source.unsplash.com/featured/?music" width="125px" height="125px" style={{ marginRight: "20px" }} />

      {props.browser && (
        <div>
          <div id="titleBrowser" style={title}>
            ALBUM
          </div>
          <h2>{props.match.params.album}</h2>
        </div>
      )}

      {!props.browser && (
        <div>
          <div id="titleList" style={title}>
            PLAYLISTS
          </div>
          <h2>{props.playlist.item.name}</h2>
          <div style={{ fontSize: "12px" }}>Created by Anderson Botega - {props.playlist.item.colSongs && <span>{props.playlist.item.colSongs.length}</span>} song(s)</div>

          <button id="btnDelete" onClick={props.onDelete} style={btnDelete}>
            Delete
          </button>
        </div>
      )}
    </div>
  );
}
