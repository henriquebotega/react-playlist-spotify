import React, { Component } from "react";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { getByID, update, getAll, remove } from "../../actions/playlistActions";
import { title, boxColSong } from "./styles";

import { withSnackbar } from "notistack";

import ItemCard from "./itemCard";
import ItemSong from "./itemSong";

class ListPage extends Component {
  componentDidMount() {
    this.loadData(this.props.match.params.id);
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.params.id !== prevProps.match.params.id) {
      this.loadData(this.props.match.params.id);
    }
  }

  loadData = id => {
    this.props.getByID(id);
  };

  onDelete = () => {
    this.props.enqueueSnackbar("Playlist successfully removed", { variant: "error" });

    this.props.remove(this.props.playlist.item);

    setTimeout(() => {
      this.props.getAll();

      this.props.history.push("/browser");
    }, 1500);
  };

  handlePlaylistDelete = song => {
    this.props.playlist.item.songs = this.props.playlist.item.songs.filter(obj => {
      return obj !== song.id;
    });

    this.props.playlist.item.colSongs = this.props.playlist.item.colSongs.filter(obj => {
      return obj.id !== song.id;
    });

    this.props.update(this.props.playlist.item);

    this.props.enqueueSnackbar("Item successfully removed", { variant: "error" });
  };

  render() {
    const { loading, items } = this.props.songs;
    const { item } = this.props.playlist;

    if (this.props.playlist.item.id) {
      const collectionSongs = this.props.playlist.item.songs;

      if (items.length > 0) {
        const songPlaylist = [];

        collectionSongs.forEach(idSong => {
          const currentSong = items.find(obj => obj.id === idSong);
          songPlaylist.push(currentSong);
        });

        this.props.playlist.item.colSongs = songPlaylist;
      }
    }

    return (
      <div>
        <ItemCard {...this.props} onDelete={this.onDelete} />

        {loading && <div>Wait...</div>}

        {item.colSongs && (
          <div style={boxColSong}>
            <div style={{ width: "75px" }}>&nbsp;</div>
            <div style={title}>TITLE</div>
            <div style={title}>ARTIST</div>
            <div style={{ width: "50px" }}></div>
          </div>
        )}

        {item.colSongs && item.colSongs.map((obj, i) => <ItemSong key={i} {...this.props} handlePlaylistDelete={this.handlePlaylistDelete} obj={obj} i={i} />)}
      </div>
    );
  }
}

const mapStateToProps = state => ({ playlist: state.playlistReducer, songs: state.songReducer });
const mapDispatchToProps = dispatch => bindActionCreators({ getByID, update, getAll, remove }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withSnackbar(ListPage));
