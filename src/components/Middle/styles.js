export const ContainerBrowser = {
  display: "flex",
  flexDirection: "column"
};

export const boxRoute = {
  overflowY: "auto",
  height: "100%"
};

export const containerSearch = {
  display: "flex",
  justifyContent: "space-between",
  marginBottom: "10px"
};

export const boxSearch = {
  display: "flex",
  flexDirection: "row"
};

export const boxSearchBack = {
  textDecoration: "none",
  color: "gray"
};
export const boxSearchField = {
  fontSize: "12px",
  fontWeight: "bold"
};

export const avatarSearch = {
  borderRadius: "25px",
  marginRight: "5px"
};

export const boxAvatarSearch = {
  display: "flex",
  flexDirection: "row"
};

export const Container = {
  flex: 1,
  padding: "10px",
  display: "flex",
  flexDirection: "column",
  backgroundColor: "#222222"
};

export const subMenu = {
  display: "flex",
  flexDirection: "row",
  margin: "10px"
};

export const boxFriendsSongs = {
  display: "flex",
  flexWrap: "wrap",
  margin: "10px"
};

export const boxFriendsDetails = {
  display: "flex",
  flexDirection: "column",
  alignItems: "start",
  fontSize: "12px"
};

export const boxFriendsDetailsTitle = {
  fontSize: "14px",
  fontWeight: "bold"
};

export const btnDelete = {
  marginTop: "15px",
  padding: "5px",
  color: "white",
  width: "100px",
  borderWidth: 0,
  backgroundColor: "darkred",
  borderRadius: "10px"
};

export const opcaoMenu = {
  textDecoration: "none",
  padding: "5px",
  color: "gray",
  fontWeight: "bold",
  fontSize: "13px"
};

export const campoBusca = {
  display: "flex",
  backgroundColor: "white",
  borderRadius: "25px",
  padding: "2px",
  marginLeft: "10px",
  width: "250px",
  color: "black",
  alignItems: "center"
};

export const title = {
  flex: 1,
  letterSpacing: "2px",
  color: "gray",
  fontWeight: "lighter",
  fontSize: "13px"
};

export const titleSong = {
  flex: 1,
  whiteSpace: "nowrap",
  overflow: "hidden",
  textOverflow: "ellipsis",
  letterSpacing: "2px",
  color: "white",
  paddingRight: "5px",
  fontWeight: "bold",
  fontSize: "13px"
};

export const browserMenu = {
  marginRight: "20px",
  letterSpacing: "2px",
  color: "gray",
  fontWeight: "lighter",
  fontSize: "12px"
};

export const friendAcitivy = {
  display: "flex",
  color: "white",
  width: "95%",
  fontWeight: "bold",
  padding: "10px",
  borderBottomWidth: 1,
  borderBottomColor: "gray",
  borderBottomStyle: "solid"
};

export const quadroSong = {
  width: "170px",
  marginRight: "15px",
  marginBottom: "15px"
};

export const boxItemCard = {
  width: "100%",
  display: "flex",
  marginTop: "20px",
  flexDirection: "row",
  alignItems: "center"
};

export const heartPlaylist = {
  width: "75px",
  textAlign: "center"
};

export const boxFavorite = {
  width: "75px",
  textAlign: "center"
};

export const boxColSong = {
  display: "flex",
  alignItems: "center",
  flexDirection: "row",
  marginTop: "10px",
  justifyContent: "space-between",
  borderBottomWidth: 1,
  borderBottomColor: "white",
  borderBottomStyle: "solid"
};

export const btnsPlaylist = {
  width: "50px",
  color: "gray",
  fontSize: "13px"
};

export const boxSong = {
  display: "flex",
  marginTop: "10px",
  paddingBottom: "10px",
  alignItems: "center",
  flexDirection: "row",
  justifyContent: "space-between",
  borderBottomWidth: 1,
  borderBottomColor: "gray",
  borderBottomStyle: "dotted"
};
