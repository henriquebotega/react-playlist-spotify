import React, { Component } from "react";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { getAll } from "../../actions/songActions";

import { Container, containerSearch, boxAvatarSearch, boxSearch, boxRoute, avatarSearch, boxSearchField, boxSearchBack, campoBusca } from "./styles";
import { ArrowBackIos, ArrowForwardIos, ArrowDownward, Search } from "@material-ui/icons";

import BrowserListPage from "./browserlist";
import BrowserPage from "./browser";
import ListPage from "./list";

import { Switch, Route, Link } from "react-router-dom";

class Middle extends Component {
  componentDidMount() {
    this.props.getAll();
  }

  renderSearch = () => {
    return (
      <div style={containerSearch}>
        <div style={boxSearch}>
          <Link to="/browser" style={boxSearchBack}>
            <ArrowBackIos />
          </Link>

          <ArrowForwardIos style={{ color: "gray" }} />
          <div style={campoBusca}>
            <Search /> <span style={boxSearchField}>Search</span>
          </div>
        </div>

        <div style={boxAvatarSearch}>
          <img alt="" src="https://source.unsplash.com/featured/?face" width="25px" height="25px" style={avatarSearch} />
          Anderson Botega
          <ArrowDownward />
        </div>
      </div>
    );
  };

  render() {
    return (
      <div style={Container}>
        {this.renderSearch()}

        <div style={boxRoute}>
          <Switch>
            <Route exact path="/" component={BrowserPage} />
            <Route path="/browser" component={BrowserPage} />
            <Route path="/browserlist/:album" component={BrowserListPage} />
            <Route path="/list/:id" component={ListPage} />
          </Switch>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  songs: state.songReducer
});

const mapDispatchToProps = dispatch => bindActionCreators({ getAll }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Middle);
