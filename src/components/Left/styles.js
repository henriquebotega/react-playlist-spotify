export const Container = {
  width: "250px",
  display: "flex",
  flexDirection: "column",
  backgroundColor: "#111111"
};

export const title = {
  marginTop: "10px",
  letterSpacing: "2px",
  color: "gray",
  padding: "5px",
  fontWeight: "lighter",
  fontSize: "13px"
};

export const opcaoMenu = {
  textDecoration: "none",
  padding: "5px",
  color: "gray",
  fontWeight: "bold",
  fontSize: "13px"
};

export const newPlaylist = {
  display: "flex",
  color: "gray",
  justifyContent: "center",
  alignItems: "center",
  cursor: "pointer",
  padding: "15px",
  borderTopWidth: 1,
  borderTopColor: "gray",
  borderTopStyle: "solid"
};

export const btnClose = {
  color: "white",
  position: "absolute",
  top: "10px",
  right: "10px"
};

export const chooseImage = {
  width: "200px",
  height: "200px",
  display: "flex",
  fontSize: "12px",
  justifyContent: "center",
  flexDirection: "column",
  alignItems: "center",
  backgroundColor: "#222222",
  marginRight: "25px"
};

export const boxNewPlaylist = {
  display: "flex",
  padding: "10px",
  borderRadius: "15px",
  backgroundColor: "#333333",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center"
};

export const boxNewPlaylistForm = {
  display: "flex",
  flexDirection: "column"
};

export const boxChooseImage = {
  display: "flex",
  flexDirection: "row"
};

export const boxScroll = {
  overflowY: "auto",
  height: "100%",
  padding: "0 15px"
};

export const linkPage = {
  textDecoration: "none",
  display: "flex",
  alignItems: "center",
  padding: "5px",
  color: "gray",
  fontWeight: "bold"
};

export const modalTitle = {
  margin: "15px",
  fontWeight: "bold"
};

export const boxName = {
  color: "gray",
  fontSize: "14px"
};

export const fieldName = {
  color: "black",
  width: "300px",
  padding: "5px",
  backgroundColor: "white",
  marginBottom: "15px"
};

export const boxDescription = {
  color: "gray",
  fontSize: "14px"
};

export const fieldDescription = {
  color: "black",
  width: "300px",
  padding: "5px",
  backgroundColor: "white"
};

export const btnCreate = {
  margin: "15px",
  padding: "5px",
  color: "white",
  width: "100px",
  borderWidth: 0,
  backgroundColor: "green",
  borderRadius: "10px"
};
