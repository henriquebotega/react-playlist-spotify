import React, { Component } from "react";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { getAll, insert } from "../../actions/playlistActions";

import { Container, boxNewPlaylist, modalTitle, boxChooseImage, boxNewPlaylistForm, boxScroll, linkPage, btnClose, title, opcaoMenu, newPlaylist, chooseImage, boxName, fieldName, boxDescription, fieldDescription, btnCreate } from "./styles";
import { Close, HomeOutlined, RadioOutlined, LibraryMusic, Public, ControlPoint } from "@material-ui/icons";

import { withSnackbar } from "notistack";
import { Link } from "react-router-dom";
import "./modal.css";

const Modal = ({ handleClose, show, children }) => {
  return (
    <div className={show ? "modal display-block" : "modal display-none"}>
      <section className="modal-main">
        <Close style={btnClose} onClick={handleClose} />
        {children}
      </section>
    </div>
  );
};

class Left extends Component {
  state = {
    show: false,
    name: "",
    description: ""
  };

  showModal = () => {
    this.setState({ show: true });
  };

  hideModal = () => {
    this.setState({ show: false });
  };

  componentDidMount() {
    this.props.getAll();
  }

  preencheCampo = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  salvarModal = e => {
    e.preventDefault();

    this.props.enqueueSnackbar("Playlist successfully created", { variant: "success" });

    this.props.insert({
      name: this.state.name,
      description: this.state.description,
      songs: []
    });

    this.setState({ name: "", description: "" });
    this.hideModal();

    setTimeout(() => {
      this.props.getAll();
    }, 1500);
  };

  renderItemPlaylist = (obj, i) => {
    return (
      <div key={i}>
        <Link style={opcaoMenu} to={`/list/${obj.id}`}>
          {obj.name}
        </Link>
      </div>
    );
  };

  render() {
    const { items } = this.props.playlists;

    return (
      <div style={Container}>
        <div style={{ padding: "10px" }}>
          <Link to="/browser" style={linkPage}>
            <HomeOutlined style={{ marginRight: "10px" }} /> Home
          </Link>

          <Link to="/browser" style={linkPage}>
            <Public style={{ marginRight: "10px" }} /> Browser
          </Link>

          <Link to="/browser" style={linkPage}>
            <RadioOutlined style={{ marginRight: "10px" }} /> Radio
          </Link>
        </div>

        <div style={boxScroll}>
          <div>
            <div style={title}>YOUR LIBRARY</div>
            <div style={opcaoMenu}>Made for you</div>
            <div style={opcaoMenu}>Recently Player</div>
            <div style={opcaoMenu}>Liked Songs</div>
            <div style={opcaoMenu}>Albums</div>
            <div style={opcaoMenu}>Artists</div>
            <div style={opcaoMenu}>Podcasts</div>
          </div>
          <div>
            <div style={title}>PLAYLISTS</div>

            {items.map((obj, i) => this.renderItemPlaylist(obj, i))}
          </div>
        </div>

        <div style={newPlaylist} onClick={this.showModal}>
          <ControlPoint style={{ marginRight: "5px" }} /> New Playlist
        </div>

        <Modal show={this.state.show} handleClose={this.hideModal}>
          <form onSubmit={this.salvarModal}>
            <div style={boxNewPlaylist}>
              <div style={modalTitle}>Create playlist</div>

              <div style={boxChooseImage}>
                <div style={chooseImage}>
                  <LibraryMusic />
                  Choose image
                </div>

                <div style={boxNewPlaylistForm}>
                  <div style={boxName}>
                    Name <br />
                    <input type="text" style={fieldName} name="name" value={this.state.name} onChange={this.preencheCampo} />
                  </div>

                  <div style={boxDescription}>
                    Description <br />
                    <textarea style={fieldDescription} name="description" value={this.state.description} onChange={this.preencheCampo} rows="7"></textarea>
                  </div>
                </div>
              </div>

              <button type="submit" style={btnCreate}>
                Create
              </button>
            </div>
          </form>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  playlists: state.playlistReducer
});

const mapDispatchToProps = dispatch => bindActionCreators({ getAll, insert }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withSnackbar(Left));
