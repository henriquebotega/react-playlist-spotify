import { combineReducers } from "redux";

import { songReducer } from "./songReducer";
import { playlistReducer } from "./playlistReducer";

export const rootReducer = combineReducers({
  songReducer,
  playlistReducer
});
