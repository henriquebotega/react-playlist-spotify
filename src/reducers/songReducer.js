const INITIAL_STATE = {
  items: [],
  item: {},
  loading: false,
  error: false
};

export const songReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    /* GET ALL */
    case "SONG_REQUEST_GETALL":
      return {
        ...state,
        loading: true,
        error: false,
        item: {},
        items: []
      };
    case "SONG_SUCCESS_GETALL":
      return {
        ...state,
        loading: false,
        items: action.items
      };
    case "SONG_FAILURE_GETALL":
      return {
        ...state,
        items: [],
        loading: false,
        error: true
      };

    /* BY ID */
    case "SONG_REQUEST_BYID":
      return {
        ...state,
        item: {},
        loading: true,
        error: false
      };
    case "SONG_SUCCESS_BYID":
      return {
        ...state,
        item: action.item,
        loading: false,
        error: false
      };
    case "SONG_FAILURE_BYID":
      return {
        ...state,
        item: {},
        loading: false,
        error: true
      };
    default:
      return state;
  }
};
