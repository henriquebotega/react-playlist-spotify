const INITIAL_STATE = {
  items: [],
  item: {},
  loading: false,
  error: false
};

export const playlistReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    /* GET ALL */
    case "PLAYLIST_REQUEST_GETALL":
      return {
        ...state,
        loading: true,
        error: false,
        items: []
      };
    case "PLAYLIST_SUCCESS_GETALL":
      return {
        ...state,
        loading: false,
        error: false,
        items: ordenar(action.items)
      };
    case "PLAYLIST_FAILURE_GETALL":
      return {
        ...state,
        loading: false,
        error: true,
        items: []
      };

    /* BY ID */
    case "PLAYLIST_REQUEST_BYID":
      return {
        ...state,
        loading: true,
        error: false,
        item: {}
      };
    case "PLAYLIST_SUCCESS_BYID":
      return {
        ...state,
        loading: false,
        error: false,
        item: action.item
      };
    case "PLAYLIST_FAILURE_BYID":
      return {
        ...state,
        loading: false,
        error: true,
        item: {}
      };

    /* DELETE */
    case "PLAYLIST_REQUEST_DELETE":
      return { ...state, error: false };
    case "PLAYLIST_SUCCESS_DELETE":
      let deleteItems = [
        ...state.items.filter(currentObj => {
          return currentObj.id !== action.item.id;
        })
      ];

      return {
        ...state,
        items: ordenar(deleteItems),
        error: false
      };
    case "PLAYLIST_FAILURE_DELETE":
      return {
        ...state,
        error: true
      };

    /* INSERT */
    case "PLAYLIST_REQUEST_INSERT":
      return { ...state, error: false };
    case "PLAYLIST_SUCCESS_INSERT":
      let insertItems = [...state.items, action.item];

      return {
        ...state,
        items: ordenar(insertItems),
        error: false
      };
    case "PLAYLIST_FAILURE_INSERT":
      return {
        ...state,
        error: true
      };

    /* UPDATE */
    case "PLAYLIST_REQUEST_UPDATE":
      return { ...state, error: false };
    case "PLAYLIST_SUCCESS_UPDATE":
      let updateItems = [
        ...state.items.filter(currentObj => {
          return currentObj.id !== action.item.id;
        }),
        action.item
      ];

      return {
        ...state,
        items: ordenar(updateItems),
        error: false
      };
    case "PLAYLIST_FAILURE_UPDATE":
      return {
        ...state,
        error: true
      };

    default:
      return state;
  }
};

const ordenar = colDados => {
  return colDados.sort(function(item1, item2) {
    return item1["id"] - item2["id"];
  });
};
