export const ContainerRow = {
  display: "flex",
  flexDirection: "row",
  color: "#ffffff",
  height: "100%"
};

export const ContainerColumn = {
  display: "flex",
  height: "100vh",
  flexDirection: "column",
  justifyContent: "space-between",
  color: "#ffffff"
};
