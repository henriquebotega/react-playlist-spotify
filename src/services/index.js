import axios from "axios";

export const ioURL = process.env.REACT_APP_API_URL;

export const api = axios.create({
  // baseURL: "http://localhost:5000"
  baseURL: ioURL
});
