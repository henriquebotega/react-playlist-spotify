import { api } from "../services";

export const getAll = () => {
  return dispatch => {
    dispatch({ type: "SONG_REQUEST_GETALL" });

    api
      .get("/library")
      .then(res => {
        dispatch({ type: "SONG_SUCCESS_GETALL", items: res.data });
      })
      .catch(error => {
        dispatch({ type: "SONG_FAILURE_GETALL" });
      });
  };
};

export const getByID = id => {
  return dispatch => {
    dispatch({ type: "SONG_REQUEST_BYID" });

    api
      .get("/library/" + id)
      .then(res => {
        dispatch({ type: "SONG_SUCCESS_BYID", item: res.data });
      })
      .catch(error => {
        dispatch({ type: "SONG_FAILURE_BYID" });
      });
  };
};
