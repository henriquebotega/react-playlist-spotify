import { api } from "../services";

export const getAll = () => {
  return dispatch => {
    dispatch({ type: "PLAYLIST_REQUEST_GETALL" });

    api
      .get("/playlist")
      .then(res => {
        dispatch({ type: "PLAYLIST_SUCCESS_GETALL", items: res.data });
      })
      .catch(error => {
        dispatch({ type: "PLAYLIST_FAILURE_GETALL" });
      });
  };
};

export const getByID = id => {
  return dispatch => {
    dispatch({ type: "PLAYLIST_REQUEST_BYID" });

    api
      .get("/playlist/" + id)
      .then(res => {
        dispatch({ type: "PLAYLIST_SUCCESS_BYID", item: res.data });
      })
      .catch(error => {
        dispatch({ type: "PLAYLIST_FAILURE_BYID" });
      });
  };
};

export const remove = item => {
  return dispatch => {
    dispatch({ type: "PLAYLIST_REQUEST_DELETE" });

    api
      .delete("/playlist/" + item.id)
      .then(res => {
        dispatch({ type: "PLAYLIST_SUCCESS_DELETE", item });
      })
      .catch(error => {
        dispatch({ type: "PLAYLIST_FAILURE_DELETE" });
      });
  };
};

export const insert = item => {
  return dispatch => {
    dispatch({ type: "PLAYLIST_REQUEST_INSERT" });

    api
      .post("/playlist", item)
      .then(res => {
        dispatch({ type: "PLAYLIST_SUCCESS_INSERT", item: res.data });
      })
      .catch(error => {
        dispatch({ type: "PLAYLIST_FAILURE_INSERT" });
      });
  };
};

export const update = item => {
  return dispatch => {
    dispatch({ type: "PLAYLIST_REQUEST_UPDATE" });

    api
      .post("/playlist/" + item.id, item)
      .then(res => {
        dispatch({ type: "PLAYLIST_SUCCESS_UPDATE", item: item });
      })
      .catch(error => {
        dispatch({ type: "PLAYLIST_FAILURE_UPDATE" });
      });
  };
};
