import React from "react";
import { connect } from "react-redux";

import Right from "./components/Right";
import Left from "./components/Left";
import Middle from "./components/Middle";
import Bottom from "./components/Bottom";

import { ContainerRow, ContainerColumn } from "./styles";
import { BrowserRouter as Router } from "react-router-dom";

const App = props => {
  if (props.playlists.error || props.songs.error) {
    return <div>The server is offline</div>;
  }

  return (
    <Router>
      <div style={ContainerColumn}>
        <div style={{ height: "calc(100vh - 75px)" }}>
          <div style={ContainerRow}>
            <Left />
            <Middle />
            <Right />
          </div>
        </div>

        <div style={{ height: "75px" }}>
          <Bottom />
        </div>
      </div>
    </Router>
  );
};

const mapStateToProps = state => ({
  playlists: state.playlistReducer,
  songs: state.songReducer
});

export default connect(mapStateToProps)(App);
